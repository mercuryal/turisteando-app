<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noticia;
use Exception;

class NewsController extends Controller
{
    public function getNews()
    {
        $news = Noticia::all();

        return response()->json([
            'status' => 200,
            'noticias' => $news
        ]);
    }

    public function getLastNews()
    {
        $news = Noticia::orderBy('created_at', 'desc')
               ->take(3)
               ->get();

        return response()->json([
            'status' => 200,
            'noticias' => $news
        ]);  

    }

    public function saveNew(Request $request)
    {
        try{
            $new = new Noticia;
            $new->tittle = $request->input('tittle');
            $new->video_url = $request->input('video_url');
            $new->content = $request->input('content');

        /*    $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);

            $new->image = url('/')."/images/".$imageName;
        */  

            $new->image = $this->storeImage($request->image);
            $new->save();

            return response()->json([
                'status' => 200,
                'noticia' => $new,
                'message' => 'Noticia guardada de manera exitosa' 
            ]);
        }
        catch(Exception $e)
        {
            return response()->json([
                'status' => 500,
                'message' => 'Error interno del servidor: '.$e
            ]);
        }
    }

    public function editNew(Request $request, $id)
    {
        try{
            $new = Noticia::find($id);
            $new->tittle = $request->input('tittle');
            $new->video_url = $request->input('video_url');
            $new->content = $request->input('content');

            $new->image = $this->storeImage($request->image);

            $new->update();

            return response()->json([
                'status' => 200,
                'noticia' => $new,
                'message' => 'Noticia actualizada de manera exitosa'
            ]);
        }
        catch(Exception $e)
        {
            return response()->json([
                'status' => 500,
                'message' => 'Error interno del servidor: '.$e 
            ]);
        }
    }

    public function showNew($id)
    {
        try{
            $new = Noticia::find($id);
            
            if($new != null)
            {
                return response()->json([
                'status' => 200,
                'noticia' => $new
                ]);
            }
            else
            {
                return response()->json([
                'status' => 404,
                'message' => 'Not found'
                ]);
            }
        }
        catch(Exception $e)
        {
            return response()->json([
                'status' => 500,
                'message' => 'Error interno del servidor: '.$e 
            ]);
        }
    }

    public function deleteNew($id)
    {
        try{
            $new = Noticia::find($id);
            
            if($new != null)
            {
                $new->delete();

                return response()->json([
                'status' => 200,
                'message' => 'Noticia eliminada'
                ]);
            }
            else
            {
                return response()->json([
                'status' => 404,
                'message' => 'Not found'
                ]);
            }
        }
        catch(Exception $e)
        {
            return response()->json([
                'status' => 500,
                'message' => 'Error interno del servidor: ' 
            ]);
        }
    }

    public function storeImage($image)
    {
        try{
            $exploded = explode(',', $image);
    
            $decoded = base64_decode($exploded[1]);

            if(str_contains($exploded[0], 'jpeg'))
                $extension = 'jpg';
            if(str_contains($exploded[0], 'png'))
                $extension = 'png';
            
            if(str_contains($exploded[0], 'jpeg') || str_contains($exploded[0], 'png'))
            {
                $filename = $this->random_string(6).'.'.$extension;
                $path = public_path().'/images/'.$filename;
                
                file_put_contents($path, $decoded);
                $urlPath = url('/')."/images/".$filename;
                return $urlPath;
            }
            else
            {
                return null;
            }
        }
        catch(Exception $e)
        {
            return $e;
        }
    }

    public function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
        }


        return $key;
    }
}
