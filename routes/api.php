<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/noticias', 'NewsController@getNews');
Route::get('/noticias/nuevas', 'NewsController@getLastNews');
Route::post('/noticia/nueva', 'NewsController@saveNew');
Route::post('/noticia/{id}/editar', 'NewsController@editNew');
Route::get('/noticia/{id}', 'NewsController@showNew');
Route::post('/noticia/{id}/eliminar', 'NewsController@deleteNew');
