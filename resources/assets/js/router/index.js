import Vue from 'vue'
import VueRouter from 'vue-router'

import List from '../views/list.vue'
import Create from '../views/create.vue'
import Edit from '../views/edit.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        {path: '/', component: List},
        {path: '/crear', component: Create},
        {path: '/edit/:id', component: Edit}
    ]
})

export default router